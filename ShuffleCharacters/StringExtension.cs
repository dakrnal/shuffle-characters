﻿using System;

namespace ShuffleCharacters
{
    public static class StringExtension
    {
        /// <summary>
        /// Shuffles characters in source string according some rule.
        /// </summary>
        /// <param name="source">The source string.</param>
        /// <param name="count">The count of iterations.</param>
        /// <returns>Result string.</returns>
        /// <exception cref="ArgumentNullException">Source string is null.</exception>
        /// <exception cref="ArgumentException">Source string is empty or a white space.</exception>
        /// <exception cref="ArgumentException">Count of iterations is less than 0.</exception>
        public static string ShuffleChars(string? source, int count)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (string.IsNullOrWhiteSpace(source))
            {
                throw new ArgumentException("avc");
            }

            if (source == string.Empty)
            {
                throw new ArgumentException("abc");
            }

            if (count < 0)
            {
                throw new ArgumentException("abc");
            }

            char[] masStr = source.ToCharArray();
            int odd = (masStr.Length / 2) + (masStr.Length % 2);
            int even = masStr.Length / 2;

            string checkStr = source;
            char[] masOdd = new char[odd];
            char[] masEven = new char[even];
            int oddCnt = 0;
            int evenCnt = 0;
            int step = 0;
            for (int i = count; i > 0; i--)
            {
                oddCnt = 0;
                evenCnt = 0;
                for (int j = 0; j < masStr.Length; j++)
                {
                    if (j % 2 == 0)
                    {
                        masOdd[oddCnt] = masStr[j];
                        oddCnt++;
                    }
                    else
                    {
                        masEven[evenCnt] = masStr[j];
                        evenCnt++;
                    }
                }

                string left = new string(masOdd);
                string right = new string(masEven);
                source = left + right;
                masStr = source.ToCharArray();
                step++;
                if (source == checkStr)
                {
                    i = count % step;
                    i++;
                }
            }

            return source;
        }
    }
}
